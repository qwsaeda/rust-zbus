// trait Foo1 {
//     fn bad<T>(&self, x: T);
//     fn new() - > Self;
// }
// //对象安全的 trait ， 将不安全的方法拆分 出 去
//  trait Foo {
//    fn bad<T>(&self , x : T) ;
//
//   trait Foo : Bar {
//       fn new() - > Self;
//   }
//对象安全的 trait ，使用 where 子 句


use std::ops::Deref;

fn main() {
    let p = Person;
    &p.who();
}


struct Person;

struct Human;

impl Deref for Person {
    type Target = Human;

    fn deref(&self) -> Box<Human> {
        Box::new(Human)
    }
}

impl Human {
    fn who(&self) {
        println!("human")
    }
}