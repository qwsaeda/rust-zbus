use std::fmt::Debug;
fn main() {
    let p = test::<Person>();
}

fn test<F>() -> impl Debug {
   Person
}

trait Fly {}

#[derive(Debug)]
struct Person;

impl Fly for Person {}