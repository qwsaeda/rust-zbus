use std::time::{Duration, SystemTime};

fn main() {
    let time = SystemTime::now();
    std::thread::sleep(Duration::from_secs(5));
    match SystemTime::now().duration_since(time) {
        Ok(n) => println!("1970-01-01 00:00:00 UTC was {} seconds ago!", n.as_secs()),
        Err(_) => panic!("SystemTime before UNIX EPOCH!"),
    }
}