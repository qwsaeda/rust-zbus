use std::sync::Arc;
use std::thread;
use std::time::Duration;

use serde_json::{Error, Value};

use zbus::{
    client::{
        RpcClient, WsRpcClient,
    }
    , message::Request,
};
use zbus::log_config;
use zbus::rpc::IOHandlers;
use zbus::service::WsRpcServer;
use zbus::wsocket::Instruct;
use zbus_lib::log_config::init_log_config;
use zbus_lib::rpc::IOHandlers;
use zbus_lib::service::WsRpcServer;

///注册为rpc服务
fn main() {
    init_log_config().unwrap();
    let mut io_handler = IOHandlers::new(10);
    io_handler.add_method("/face/faceCheck/extractFaceFeature".into(), |mut params: Value| {
        println!("invoke");
        Ok(if params.is_array() {// TODO 这块的代码都可以用宏来消防模版代码
            let params = params.as_array_mut().unwrap();
            params.reverse();
            let a = params.pop().unwrap();
            let b = params.pop().unwrap();
            let a = serde_json::from_value(a).unwrap();
            let b = serde_json::from_value(b).unwrap();
            let result = add(a, b);
            println!("{}+{}={}", a, b, result);
            Value::from(result)
        } else {
            Value::Null
        })
    });
    let rpc_server = WsRpcServer::connect("ws://10.0.200.98:15555", "/face".into(), io_handler);
    rpc_server.registry_service();
    println!("准备结束");
    thread::sleep(Duration::from_secs(100));
    println!("stop");
}

pub fn add(a: i32, b: i32) -> i32 {
    a + b
}