use std::future::Future;

fn main() {
    let handle: Box<dyn Future<Output=String>> = get_handler();
}

fn get_handler() -> Box<Future<Output=String>>
{
    let f = async { "name".to_string() };
    Box::new(f)
}
