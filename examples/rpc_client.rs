use std::thread;
use std::time::Duration;

use serde_json::Value;

use zbus::{
    client::{
        RpcClient, WsRpcClient,
    }
    , message::Request,
};

/// rpc client调用远程服务例子
fn main() {
    let rpc_client = WsRpcClient::connect("ws://10.0.200.98:15555");
    for i in 0..2 {
        let ws_conn = rpc_client.handler();
        thread::spawn(move || {
            let req = Request::builder()
                .url("/corp/user/findAll")
                .method("GET")
                .body(Value::Array(vec!["username".into(), "mzywx".into()]))
                .id(format!("0002f6eca3fc42c6812d23dd73cbed{}", i))
                .build();
            match ws_conn.invoke(req) {
                Ok(resp) => {
                    println!("{:?}", resp);
                }
                Err(e) => {
                    println!("{:?}", e);
                }
            }
        });
    }
    println!("准备结束");
    thread::sleep(Duration::from_secs(100));
    println!("stop");
}