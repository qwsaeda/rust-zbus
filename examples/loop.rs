struct MessageHandler {
    handlers: Vec<Arc<dyn Handler + Sync + Send + 'static>>,
}


impl MessageHandler {
    fn add_handler<H>(&mut self, handler: H) where H: Handler + Send + Sync + 'static {
        self.handlers.push(Arc::new(handler));
    }
    fn handle(&self) {
        for i in 0..25 {
            let message = String::from("zxj");
            self.message_handler(message);
        }
    }
    fn message_handler(&self, message: String) {
        for handler in &self.handlers {
            let handler = handler.clone();
            let message = message.clone();
            thread::spawn(move || {
                handler.handle(message);
            });
        }
    }
}

use std::thread;
use std::sync::Arc;
use std::future::Future;

struct InstrctHandler {}

impl Handler for InstrctHandler {
    fn handle(&self, message: String) {
        println!("instrct handle is {}", message);
    }
}

struct ActiveHandler {}

impl Handler for ActiveHandler {
    fn handle(&self, message: String) {
        println!("active handle is {}", message);
    }
}

fn main() {
    let mut msg_handler = MessageHandler {
        handlers: Vec::new()
    };
    msg_handler.add_handler(ActiveHandler {});
    msg_handler.add_handler(InstrctHandler {});
    msg_handler.handle();
}

trait Handler {
    fn handle(&self, message: String) {
        println!("msg is {}", message);
    }
}

fn get_handler<F, H>() -> F
    where F: Fn() -> H,
          H: Future<Output=String>,
{
    sync || "name".to_string()
}