use log::{debug, error, info, LevelFilter, SetLoggerError, trace, warn};
use log4rs::{
    append::{
        console::{ConsoleAppender, Target},
        file::FileAppender,
    },
    config::{Appender, Config, Root},
    encode::pattern::PatternEncoder,
    filter::threshold::ThresholdFilter,
};

// 为配合docker 配置改为环境变量方式和命令行参数方式
pub fn init_log_config() -> Result<(), SetLoggerError> {
    let level = log::LevelFilter::Debug;
    let file_path = "d:/tmp/foo.log";

    // Build a stderr logger.
    let stderr = ConsoleAppender::builder().encoder(
        Box::new(PatternEncoder::new("[{d(%Y-%m-%d %H:%M:%S)} {T} {h({l})} {M}/{f}:{L}] - {m}\n"))
    ).target(Target::Stderr).build();

    // Logging to log file.
    let logfile = FileAppender::builder()
        // Pattern: https://docs.rs/log4rs/*/log4rs/encode/pattern/index.html
        .encoder(Box::new(PatternEncoder::new("[{d(%Y-%m-%d %H:%M:%S)} {T} {h({l})} {M}/{f}:{L}] - {m}\n")))
        .build(file_path)
        .unwrap();

    // Log Trace level output to file where trace is the default level
    // and the programmatically specified level to stderr.
    let config = Config::builder()
        .appender(Appender::builder().build("logfile", Box::new(logfile)))
        .appender(
            Appender::builder()
                .filter(Box::new(ThresholdFilter::new(level)))
                .build("stderr", Box::new(stderr)),
        )
        .build(
            Root::builder()
                .appender("logfile")
                .appender("stderr")
                .build(LevelFilter::Trace),
        )
        .unwrap();

    let _handle = log4rs::init_config(config)?;
    Ok(())
}