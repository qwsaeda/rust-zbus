use std::thread;
use std::time::Duration;

// pub use wsocket::{WsClient, Instruct, WsClientHandler};
use serde::de::DeserializeOwned;
use serde::Deserialize;
use serde_json::Value;

pub use component::WsRpcClient;

use crate::{
    err::ZbusErr,
    message::{Request, Response},
};
use crate::err::ZbusResult;

mod component;

// mod rpc;

// mod wsocket;

pub trait RpcClient {
    fn invoke(&self, req: Request) -> ZbusResult<Value> {
        self.deliver(req).and_then(|resp| {
            if resp.status() == 200 {
                Ok(resp.body())
            } else {
                Err(ZbusErr::err(resp.body().to_string()))
            }
        })
    }
    // fn invoke<T: for<'de> Deserialize<'de>> (&self, req: Request) -> ZbusResult<T> {
    //     self.deliver(req).and_then(|resp| {
    //         if resp.status() == 200 {
    //             serde_json::from_value(resp.body()).map_err(|e|From::from(e))
    //         } else {
    //             Err(ZbusErr::err(resp.body().to_string()))
    //         }
    //     })
    // }
    fn deliver(&self, req: Request) -> ZbusResult<Response>;
}


pub trait WebsocketClient {
    fn connect(&self) -> bool;
    fn reconnect(&mut self) -> bool;
    fn message<S: Into<String>>(&self, id: S) -> ws::Message;
    fn is_connected(&self) -> bool;
    fn close(&self) -> bool;
    fn send(&self, msg: ws::Message) -> bool;
}


// #[test]
// fn client() -> OkResult {
//     let rpc_client = WsRpcClient::connect("ws://127.0.0.1:1555");
//     for _ in 0..20 {
//         let ws_conn = rpc_client.handler();
//         thread::spawn(move || {
//             let req = Request::builder()
//                 .url("/test")
//                 .body(Value::String("iddasfa".into()))
//                 .id("dsafasafda")
//                 .build();
//             ws_conn.deliver(req);
//         });
//     }
//     thread::sleep(Duration::from_secs(20));
//     Ok(())
// }
