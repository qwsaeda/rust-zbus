use std::collections::HashMap;
use std::sync::Arc;
use std::sync::mpsc::Sender;

use log::info;
use serde_json::Value;

use crate::client::RpcClient;
use crate::err::OkResult;
use crate::message::{Message, Request};
use crate::rpc::{IOHandlers, Protocol, WsRpcHandler};
use crate::wsocket::{Instruct, WsClient};

//创建一个MQ通道(如果这个通道被其它服务创建过了，并不会再创建或者是MQ服务分发到不同的相同名的通道)，并且订阅这个通道
// 接收到MQ消息按RPC消息处理
//对wsClient进行封装
pub struct WsRpcClient {
    ws_client: WsClient,

}


impl WsRpcClient {
    pub fn connect(url: &'static str) -> Self {
        let ws_client = WsClient::connect(url, None).unwrap();
        Self { ws_client }
    }

    pub fn handler(&self) -> WsRpcHandler {
        WsRpcHandler {
            handler: self.ws_client.handler()
        }
    }
    pub fn reconnect(&self) -> OkResult {
        self.ws_client.reconnect()
    }
}
