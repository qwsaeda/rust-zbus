use std::thread;
use std::time::{Duration, SystemTime};

use crate::client::RpcClient;
use crate::err::{ZbusErr, ZbusResult};
use crate::message::{Message, Request, Response};
use crate::wsocket::WsClientHandler;

//实现了rpclient trait并且对wsClientHandler进行封装，可以实现轻量clone
pub struct WsRpcHandler {
    pub handler: WsClientHandler,
}

impl RpcClient for WsRpcHandler {
    fn deliver(&self, req: Request) -> ZbusResult<Response> {
        match req.id() {
            Some(msg_id) => {
                let msg_id = String::from(msg_id);
                self.handler.send(Message::Request(req))?;
                let timeout = 10 * 1000;
                self.response_timeout(msg_id, timeout)
            }
            None => Err(ZbusErr::validate("request id not found")),
        }
    }
}

impl WsRpcHandler {
    fn response_timeout(&self, msg_id: String, timeout: u64) -> ZbusResult<Response> {
        // let millis = 5;
        // let num = timeout / millis;
        let time = SystemTime::now();
        loop {
            let resp = self.handler.response(msg_id.clone());
            if resp.is_ok() {
                return resp;
            } else {
                let current = match SystemTime::now().duration_since(time) {
                    Ok(n) => n.as_millis(),
                    Err(_) => break,
                };
                if current > timeout as u128 {
                    break;
                }
            }
        };
        Err(ZbusErr::time_out(" is not found response", timeout as i32))
    }

    // async fn deliver(&self, req: Request) -> ZbusResult<Response> {
    //     match req.id() {
    //         Some(msg_id) => {
    //             let msg_id = String::from(msg_id);
    //             self.handler.send(req)?;
    //             let timeout = 10 * 1000;
    //             self.response_timeout(msg_id, timeout)
    //         }
    //         None => Err(ZbusErr::Validate("request id not found".into())),
    //     }
    // }
}
