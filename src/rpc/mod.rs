use std::sync::mpsc::Sender;

pub use {
    handler::WsRpcHandler,
    io::{
        Handler, IOHandlers,
    },
    protocol::Protocol,
};

use crate::message::Request;
use crate::wsocket::Instruct as WsInstruct;

mod io;

mod handler;

mod protocol;

pub enum Instruct {
    RPCRequest(Request, Sender<WsInstruct>),
    Exit,
}
