pub struct Protocol;

impl Protocol {
    //Parameter keys(Message main key-value pairs)
    pub(crate) const CMD: &'static str = "cmd";
    // Request message command
    const STATUS: &'static str = "status";
    // Response message status
    pub(crate) const ID: &'static str = "id";
    // Message ID
    const BODY: &'static str = "body";
    // Message body
    const API_KEY: &'static str = "apiKey";
    // Authentication  Key
    const SIGNATURE: &'static str = "signature"; // Authentication signature generated

    //Command values(key=cmd)
    const PUB: &'static str = "pub";
    //Publish message
    pub(crate) const SUB: &'static str = "sub";
    //Subscribe message stream
    const TAKE: &'static str = "take";
    //One-time read message from MQ
    pub(crate) const ROUTE: &'static str = "route";
    //Route message to specified sender client
    pub(crate) const CREATE: &'static str = "create";
    //Create or Update
    const REMOVE: &'static str = "remove";
    //Remove MQ/Channel
    const QUERY: &'static str = "query";
    //Query MQ/Channel
    const PING: &'static str = "ping";     //Heartbeat ping

    pub(crate) const MQ: &'static str = "mq";
    pub(crate) const CHANNEL: &'static str = "channel";
    const FILTER: &'static str = "filter";
    //Filter on message's tag
    const TAG: &'static str = "tag";
    //Tag of message, if filter applied
    const OFFSET: &'static str = "offset";
    const CHECKSUM: &'static str = "checksum";
    //Offset checksum
    pub(crate) const SOURCE: &'static str = "source";
    //message's source id(socket)
    pub(crate) const TARGET: &'static str = "target";
    //route message's target id(socket)
    pub(crate) const MQ_TYPE: &'static str = "mqType";
    pub(crate) const MQ_MASK: &'static str = "mqMask";
    const CHANNEL_MASK: &'static str = "channelMask";
    const WINDOW: &'static str = "window";
    const ACK: &'static str = "ack";
    pub(crate) const MEMORY: &'static str = "memory";

    pub(crate) const MASK_DELETE_ON_EXIT: i32 = 1 << 0;
    const MASK_EXCLUSIVE: i32 = 1 << 1;
    const MASK_TAKEOVER: i32 = 1 << 2;   //NOT support yet
}
