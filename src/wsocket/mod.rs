use std::sync::mpsc::Sender;

use ws::{CloseCode, Handler, Sender as WsCmd};

pub use component::WsClient;
pub use handler::WsClientHandler;

use crate::err::{OkResult, ZbusErr, ZbusResult};
use crate::message::{Message, Request, Response};

mod handler;
mod component;

pub enum Instruct {
    Connected(WsCmd),
    Connect,
    Delivery(Message, Option<Sender<OkResult>>),
    //投递消息
    Receive(Message),
    //接收到消息
    Response(String, Option<Sender<ZbusResult<Response>>>),
    //获取response消息，response管理器目前放在一起
    Close(Option<Sender<OkResult>>),
    Closed,
    Exit,
    IsClose(Option<Sender<OkResult>>),
}


