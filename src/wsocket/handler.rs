use std::sync::mpsc::{channel, Sender};
use std::time::Duration;

use crate::err::{OkResult, ZbusErr};
use crate::message::{Message, Request, Response};
use crate::wsocket::Instruct;

//对wsclient的包装，可以跨线程使用客户端
pub struct WsClientHandler {
    pub tx: Sender<Instruct>,
}

impl WsClientHandler {
    pub fn is_close(&self) -> OkResult {
        Ok(())
    }
    pub fn response(&self, msg_id: String) -> Result<Response, ZbusErr> {
        let (tx, rx) = channel();
        self.excute(Instruct::Response(msg_id, Some(tx)))?;
        rx.recv_timeout(Duration::from_millis(500))?
    }
    pub fn send(&self, msg: Message) -> OkResult {
        let (tx, rx) = channel();
        self.excute(Instruct::Delivery(msg, Some(tx)))?;
        rx.recv_timeout(Duration::from_secs(10))?
    }
    fn excute(&self, instruct: Instruct) -> OkResult {
        self.tx.send(instruct).map_err(|e| ZbusErr::err(e.to_string()))
    }

// fn close(&self)->Result<(),ZbusErr>;
}
