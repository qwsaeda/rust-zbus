use std::error::Error;
use std::fmt::{
    Debug, Display,
};
use std::sync::mpsc::RecvTimeoutError;
use std::time::Duration;

pub type ZbusResult<T> = Result<T, ZbusErr>;
pub type OkResult = ZbusResult<()>;

pub struct ZbusErr {
    inner: ErrKind,
}

// pub enum ZbusErr {
//     TimeOut(String),
//     Closed,
//     Err(String),
//     Validate(String),
// }
impl ZbusErr {
    pub fn new(inner: ErrKind) -> Self {
        ZbusErr {
            inner
        }
    }
    pub fn err<E: Into<String>>(err: E) -> Self {
        ZbusErr {
            inner: ErrKind::Err(From::from(err.into()))
        }
    }
    pub fn validate<E: Into<String>>(err: E) -> Self {
        ZbusErr {
            inner: ErrKind::Validate(From::from(err.into()))
        }
    }
    pub fn time_out<E: Into<String>>(err: E, out_ms: i32) -> Self {
        ZbusErr {
            inner: ErrKind::TimeOut(From::from(err.into()), out_ms)
        }
    }
    pub fn closed() -> Self {
        ZbusErr {
            inner: ErrKind::Closed
        }
    }
}

pub enum ErrKind {
    TimeOut(String, i32),
    Closed,
    Validate(String),
    Err(Box<dyn  Error + Send + Sync>),
}

impl Debug for ZbusErr {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match &self.inner {
            ErrKind::TimeOut(ref msg, ms) => write!(f, "timeout{}={}ms", msg, ms),
            ErrKind::Closed => write!(f, "rpc client closed {}", ""),
            ErrKind::Validate(ref msg) => write!(f, "validate fail {}", msg),
            ErrKind::Err(err) => write!(f, "{}", err.to_string()),
        }
    }
}

impl Display for ZbusErr {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match &self.inner {
            ErrKind::TimeOut(ref msg, ms) => write!(f, "timeout{} {}ms", msg, ms),
            ErrKind::Closed => Display::fmt("rpc client closed {}", f),
            ErrKind::Validate(ref msg) => write!(f, "validate fail {}", msg),
            ErrKind::Err(err) => write!(f, "{}", err.to_string()),
        }
    }
}


impl From<RecvTimeoutError> for ZbusErr {
    fn from(err: RecvTimeoutError) -> Self {
        ZbusErr::new(ErrKind::TimeOut(err.to_string(), -1))
    }
}


impl<B> From<Box<B>> for ZbusErr
    where
        B: Error + Send + Sync + 'static,
{
    fn from(err: Box<B>) -> ZbusErr {
        ZbusErr::new(ErrKind::Err(err))
    }
}

impl From<serde_json::error::Error> for ZbusErr
{
    fn from(err: serde_json::error::Error) -> Self {
        Self::new(ErrKind::Err(Box::new(err)))
    }
}
