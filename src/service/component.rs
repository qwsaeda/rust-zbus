use std::collections::HashMap;
use std::sync::Arc;
use std::sync::mpsc::Sender;

use log::error;
use log::info;
use serde_json::Value;

use crate::client::RpcClient;
use crate::err::OkResult;
use crate::message::{Message, Request};
use crate::rpc::{IOHandlers, Protocol, WsRpcHandler};
use crate::wsocket::{Instruct, WsClient};

//创建一个MQ通道(如果这个通道被其它服务创建过了，并不会再创建或者是MQ服务分发到不同的相同名的通道)，并且订阅这个通道
// 接收到MQ消息按RPC消息处理
//对wsClient进行封装
pub struct WsRpcServer {
    ws_client: WsClient,
    mq: String,
    mqType: String,
    mqMask: i32,
    channel: String,

}

impl WsRpcServer {
    pub fn connect(url: &'static str, mq: String, process: IOHandlers) -> Self {
        let ws_client = WsClient::connect(url, Some(process)).unwrap();
        Self { ws_client, mq: mq.clone(), mqType: Protocol::MEMORY.into(), mqMask: Protocol::MASK_DELETE_ON_EXIT, channel: mq }
    }
    pub fn registry_service(&self) {
        let mut headers = HashMap::new();
        headers.insert(Protocol::CMD.into(), Value::from(Protocol::CREATE)); // create MQ/Channel
        headers.insert(Protocol::MQ.into(), Value::from(self.mq.clone()));
        headers.insert(Protocol::MQ_MASK.into(), Value::from(self.mqMask));
        headers.insert(Protocol::MQ_TYPE.into(), Value::from(self.mqType.clone()));
        headers.insert(Protocol::CHANNEL.into(), Value::from(self.channel.clone()));//
        let req = Request::builder().headers(headers).id("0002f6eca3fc42c6812d23dd73cbed").method("").url("").build();
        error!("req create mq {:?}", serde_json::to_string(&req));
        let resp = self.handler().deliver(req).unwrap();
        error!("{:?}", serde_json::to_string(&resp));
        let mut headers = HashMap::new();
        headers.insert(Protocol::CMD.into(), Protocol::SUB.into()); // Subscribe on MQ/Channel
        headers.insert(Protocol::MQ.into(), Value::from(self.mq.clone()));
        headers.insert(Protocol::CHANNEL.into(), Value::from(self.channel.clone()));
        let subMessage = Request::builder().headers(headers).id("0002f6eca3fc42c6812d23dd73cbed1").method("").url("").build();
        self.handler().deliver(subMessage).map(|resp| {
            error!("{:?}", serde_json::to_string(&resp));
        });
    }

    pub fn handler(&self) -> WsRpcHandler {
        WsRpcHandler {
            handler: self.ws_client.handler()
        }
    }
    pub fn reconnect(&self) -> OkResult {
        self.ws_client.reconnect()
    }
}
